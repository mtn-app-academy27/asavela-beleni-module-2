void main() {
  var appObject = new AppInformation();
  appObject.appName = 'Naked Insurance';
  appObject.sector = 'offers ene-to-end artificial inteligence';
  appObject.developer = 'Thomson';
  appObject.year = 2019;

  print(appObject.appName);
  print(appObject.sector);
  print(appObject.developer);
  print(appObject.year);

  appObject.printAppNane();
}

class AppInformation {
  String? appName;
  String? sector;
  String? developer;
  int? year;

  void printAppNane() {
    print('App name is ${appName!.toUpperCase()}');
  }
}
